import { DatePipe } from '@angular/common';
import { Component, computed, signal } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FormatDatePipe } from '../shared/pipes/format-date.pipe';

@Component({
  selector: 'app-uikit11',
  standalone: true,
  imports: [
    DatePipe,
    FormsModule,
    FormatDatePipe
  ],
  template: `
    <p>
      uikit11 works!
    </p>
    <div>1. {{today()  | formatDate}}</div>
    <div>1. {{today()  | formatDate: 2}}</div>
    <div>2. {{formatDate()}}</div>
    <div>3. {{formatDateSignal()}}</div>
    
    {{render()}}
    
    <input type="text" ngModel>
  `,
  styles: ``
})
export default class Uikit11Component {
  today = signal(Date.now())

  formatDateSignal = computed(() => {
    console.log('formatDateSignal')
    return 'abc' + this.today()
  })

  formatDate() {
    return 'abc' + this.today()
  }

  render() {
  }
}
