import { JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, Input } from '@angular/core';
import { delay } from 'rxjs';
import { Meteo } from '../../model/meteo';

@Component({
  selector: 'app-weather',
  standalone: true,
  imports: [
  ],
  template: `
    @if(meteo) {
      TEMPERATURE: {{ meteo.main.temp }}° in {{cityLabel}}
    }
  `,
  styles: ``
})
export class WeatherComponent {
  meteo: any
  cityLabel: string | undefined;
  http = inject(HttpClient)

  @Input() set city(val: string) {
    this.cityLabel = val;
    this.http.get(`https://api.openweathermap.org/data/2.5/weather?q=${val}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .pipe(delay(2000))
      .subscribe(res => {
       this.meteo = res;
      })
  }

  // ngOnChanges
}
