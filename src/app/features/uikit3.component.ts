import { AfterViewInit, Component, ElementRef, OnDestroy, ViewChild } from '@angular/core';
import { WeatherComponent } from '../uikit/components/weather.component';
import { Weather2Component } from '../uikit/components/weather2.component';
import { Weather3SignalComponent } from '../uikit/components/weather3.component';

@Component({
  selector: 'app-uikit3',
  standalone: true,
  imports: [
    WeatherComponent,
    Weather2Component,
    Weather3SignalComponent
  ],
  template: `
    <h1>Weather component </h1>
    
    <!--1.<app-weather [city]="value" />
    <br>
    2. <app-weather2 [city]="value" unit="imperial"/>-->
    <br>
    3. <app-weather3-signal [city]="value" />
    <hr>
    <button class="btn btn-info" (click)="value = 'Rome'">Rome</button>
    <button class="btn btn-info" (click)="value = 'Milan'">Milan</button>
    <button class="btn btn-info" (click)="value = 'Palermo'">Palermo</button>
    
    <input type="text" #inputRef>
  `,
  styles: ``
})
export default class Uikit3Component implements AfterViewInit, OnDestroy {
  @ViewChild('inputRef') input!: ElementRef<HTMLInputElement>

  value = 'Trieste'

  ngAfterViewInit() {
    this.input.nativeElement.focus();
  }

  ngOnDestroy() {
    console.log('destroy')
  }
}
