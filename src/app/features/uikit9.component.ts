import { NgIf } from '@angular/common';
import { Component, HostBinding } from '@angular/core';
import { RouterLink } from '@angular/router';
import { IfLoggedDirective } from '../shared/directives/if-logged.directive';
import { StopPropagationDirective } from '../shared/directives/stop-propagation.directive';
import { UrlDirective } from '../shared/directives/url.directive';
import { MarginDirective } from '../uikit/directives/margin.directive';
import { PadDirective, Size } from '../uikit/directives/pad.directive';

@Component({
  selector: 'app-uikit9',
  standalone: true,
  imports: [
    RouterLink,
    PadDirective,
    MarginDirective,
    UrlDirective,
    StopPropagationDirective,
    NgIf,
    IfLoggedDirective,
  ],
  template: `
    <h1>Directives</h1>
    
    <button  class="btn" (click)="value = 'sm'">sm</button>
    <button  class="btn" (click)="value = 'xl'">xl</button>

    <div
      appUrl="https://www.google.com"
      [appPad]="value"
    >padding example</div>
    <div [appMargin]="value">margin example</div>
   
    <div (click)="parent()" class="p-4 bg-orange-700">
      PARENT
      <div (click)="child()" appStopPropagation class="p-4 bg-sky-700">CHILD</div>
    </div>
  `,
  styles: ``
})
export default class Uikit9Component {
  url = '...'

  value: Size = 'xl'
  doSomething() {

  }

  parent() {
    console.log('parent')
  }
  child() {
    console.log('child')
  }
}
