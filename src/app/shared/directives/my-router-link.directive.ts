import { Directive, effect, ElementRef, HostBinding, inject, input, Input, Renderer2 } from '@angular/core';
import { takeUntilDestroyed } from '@angular/core/rxjs-interop';
import { NavigationEnd, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Directive({
  selector: '[appMyRouterLink]',
  standalone: true
})
export class MyRouterLinkDirective {
  appMyRouterLink = input.required<string>()
  router = inject(Router)
  el = inject(ElementRef)
  renderer = inject(Renderer2)

  constructor() {
    this.router.events
      .pipe(
        takeUntilDestroyed()
      )
      .subscribe(event => {
        if (event instanceof NavigationEnd) {
          const routerLink = this.el.nativeElement.getAttribute('routerLink')
          const classToApply = this.appMyRouterLink();

          if(event.url.includes(routerLink)) {
            this.renderer.addClass(this.el.nativeElement, classToApply)
          } else {
            this.renderer.removeClass(this.el.nativeElement, classToApply)
          }

        }
      })
  }

}
