import { JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, effect, inject, input, Input, OnChanges, OnInit, signal, SimpleChanges } from '@angular/core';
import { Meteo } from '../../model/meteo';

@Component({
  selector: 'app-weather3-signal',
  standalone: true,
  imports: [
  ],
  template: `
    WEATHER 3 SIGNAL
    @if(meteo(); as meteo) {
      TEMPERATURE: {{ meteo.main.temp }}° in {{city()}}
    }

  `,
  styles: ``
})
export class Weather3SignalComponent   {
  city = input<string>('')
  http = inject(HttpClient);
  meteo = signal<Meteo | null>(null)

  constructor() {
    effect(() => {
      this.http.get<Meteo>(`https://api.openweathermap.org/data/2.5/weather?q=${this.city()}&units=metric&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
        .subscribe(res => {
          this.meteo.set(res);
        })
    });
  }

/*
  meteo: any


  @Input() city: string | undefined;
  @Input() unit: 'metric' | 'imperial' = 'metric'

  ngOnChanges(changes: SimpleChanges){
    if (changes['city']) {
      console.log('changed CITY', changes['city'].currentValue, this.city, this.unit)
    }
    this.http.get(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .subscribe(res => {
        this.meteo = res;
      })
  }
*/


}
