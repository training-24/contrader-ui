import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { AuthService } from '../core/auth.service';

@Component({
  selector: 'app-login',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <p>
      login works!
    </p>
    <div>
      {{authSrv.data | json}}
    </div>
    
    <button class="btn" (click)="authSrv.login()">SIGN IN</button>
  `,
  styles: ``
})
export default class LoginComponent {
  authSrv = inject(AuthService)
}
