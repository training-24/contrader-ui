import { Component, computed, model, signal } from '@angular/core';
import { AccordionItemComponent } from '../uikit/components/accordion-item.component';
import { AccordionComponent } from '../uikit/components/accordion.component';
import { PicsumComponent } from '../uikit/components/picsum.component';
import { SidePanelComponent } from '../uikit/components/side-panel.component';
import { TimelineComponent, TimelineData } from '../uikit/components/timeline.component';

export const LAYOUT_COMPONENTS = [
  TimelineComponent,
  AccordionItemComponent,
  AccordionComponent,
  PicsumComponent,
  SidePanelComponent
]

@Component({
  selector: 'app-uikit1',
  standalone: true,
  imports: [
    ...LAYOUT_COMPONENTS
  ],
  template: `

{{isOpen}}
      <app-side-panel title="DRAWER" [(open)]="isOpen">
        Lorem ipsum dolor sit amet, consectetur adipisicing elit. Beatae dolorem earum molestiae officiis? Aliquam esse hic reprehenderit? Aperiam omnis, veniam. Blanditiis eius eveniet magnam nemo non placeat quas sapiente similique!
      </app-side-panel>
    
    
    
    <button (click)="isOpen = !isOpen ">toggle side panel</button>
    
    <h1>Picsum</h1>
    <app-picsum />
    <app-picsum [width]="1000" height="600" grayscale />
    
    <h1>Timeline</h1>
    <app-timeline 
      [items]="timelineList()"  
      [fn]="customFn"
    />
    <app-timeline [items]="filteredArray()" vertical />

    <!--<h1>accordion</h1>
   @for(item of timelineList(); track $index) {
     <app-accordion-item [title]="item.start" name="timelineItem">
       {{item.end}}
     </app-accordion-item>
   }-->

      @defer (on interaction) {
        <app-accordion [items]="timelineList()"></app-accordion>
      } @placeholder {
        <button>load accordion</button>
      }
    <button (click)="doNOthing()">do nothing</button>
    
    
  `,
  styles: ``
})
export default class Uikit1Component {
  isOpen = false;
  text = model('ciaone')

  timelineList = signal<TimelineData[]>([
    { start: '2014', end: 'description' },
    { start: '2014', end: 'lorem...' },
    { start: '2018', end: 'bla bla' },
    { start: '2022', end: 'another' },
    { start: '2023', end: 'hello' }
  ])

  filteredArray =  computed(() => this.timelineList().filter(item => {
    return +item.start >= 2018
  }))

  customFn(a: number, b: number) {
    return a + b;
  }

  doNOthing() {}
}
