import { Component } from '@angular/core';
import { visible } from 'ansi-colors';
import { CardService } from '../core/card.service';
import { CardComponent } from '../uikit/components/card.component';
import { TitleComponent } from '../uikit/components/title.component';

@Component({
  selector: 'app-uikit4',
  standalone: true,
  imports: [
    CardComponent,
    TitleComponent
  ],
  template: `
    <app-title>Defer demo</app-title>
   
    @defer (on interaction) {
       <app-card />
    } @placeholder {
      <button>toggle visibilty {{isVisible}}</button>
    } @loading {
      <div>rendering......</div>
    } @error {
      <div>failed to load chunk</div>
    }
  `,
  styles: ``,
  providers: [CardService]
})
export default class Uikit4Component {
  isVisible = false;
}
