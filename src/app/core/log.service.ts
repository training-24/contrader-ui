import { Injectable } from '@angular/core';

@Injectable()
export class LogService {
  counter = 1;
  logs: string[] = [];

  constructor() {
    console.log('log service constructor')
  }

  show(msg: string) {
    this.logs.push(msg + Math.random())
    this.counter++;
  }

  ngOnDestroy()  {
    console.log('destroy')
  }
}
