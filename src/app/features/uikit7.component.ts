import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { environment } from '../../environments/environment';
import { LogService } from '../core/log.service';
import { SuperLogService } from '../core/super-log.service';

@Component({
  selector: 'app-uikit7',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <h1>Super Log 2 Service</h1>
    <div>counter: {{logSrv.counter}}</div>
    <div>{{logSrv.logs | json}}</div>
    <button (click)="logSrv.show('item')">
      LOG
    </button>
  `,
  styles: ``,
  providers: [
    { provide: LogService, useExisting: SuperLogService }
  ]
})
export default class Uikit7Component {
  logSrv = inject(LogService)

  constructor() {
    console.log(environment.host)
  }
}
