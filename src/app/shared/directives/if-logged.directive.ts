import {
  Directive,
  effect,
  ElementRef,
  HostBinding,
  inject,
  Renderer2,
  TemplateRef,
  ViewContainerRef
} from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfLogged]',
  standalone: true
})
export class IfLoggedDirective {

  tpl = inject(TemplateRef)
  view = inject(ViewContainerRef)
  authService = inject(AuthService);

  constructor() {
    effect(() => {
      this.view.clear()

      if (this.authService.isLogged())
        this.view.createEmbeddedView(this.tpl)
    });
  }
}
