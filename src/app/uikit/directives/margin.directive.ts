import { computed, Directive, effect, ElementRef, inject, Input, input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appMargin]',
  standalone: true
})
export class MarginDirective {
  appMargin = input<string>()

  margin = computed(() => {
    return this.appMargin() === 'sm' ? 'm-4' : 'm-16'
  })
  el = inject(ElementRef)
  renderer = inject(Renderer2)

  constructor() {
    effect(() => {
      this.renderer.setAttribute(
        this.el.nativeElement,
        'class',
        this.margin()
      )
    });

    effect(() => {
      // ...
    });
  }

}
