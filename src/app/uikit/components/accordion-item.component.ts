import { booleanAttribute, Component, inject, Input } from '@angular/core';
import { AccordionComponent } from './accordion.component';

@Component({
  selector: 'app-accordion-item',
  standalone: true,
  imports: [],
  template: `
    <div class="collapse bg-base-200">
      <input type="radio" [name]="name" [checked]="opened" />
      <div class="collapse-title text-xl font-medium">
        {{title}}
      </div>
      <div class="collapse-content">
        <ng-content></ng-content>
      </div>
    </div>

  `,
  styles: ``
})
export class AccordionItemComponent {
  @Input() title: string | undefined
  @Input() name = 'accordion-1'
  @Input({ transform: booleanAttribute}) opened = false

  accordion = inject(AccordionComponent, { optional: true })

  constructor() {
    console.log(this.accordion?.items)
  }
}
