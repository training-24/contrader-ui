import { Directive, effect, ElementRef, inject, Renderer2 } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfSignIn]',
  standalone: true
})
export class IfSignInDirective {
  authSrv = inject(AuthService)
  el = inject(ElementRef)
  renderer = inject(Renderer2)

  constructor() {
    effect(() => {
      this.renderer.setStyle(
        this.el.nativeElement,
        'display',
        this.authSrv.isLogged() ? 'inline-block' : 'none'
      )
    });
  }
}
