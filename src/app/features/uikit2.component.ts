import { Component } from '@angular/core';
import { AccordionItemComponent } from '../uikit/components/accordion-item.component';

@Component({
  selector: 'app-uikit2',
  standalone: true,
  imports: [
    AccordionItemComponent
  ],
  template: `
  
    <h1>Accordion 1</h1>
    <app-accordion-item  title="one">
      content 1
      <input type="text">
    </app-accordion-item>
    
    <app-accordion-item  title="two" opened>
      content 2  
    </app-accordion-item>
    
    <app-accordion-item  title="three">
      content 3  
    </app-accordion-item>


    <h1>Accordion 2</h1>
    <app-accordion-item  title="one" name="accordion2" opened>
      content 12
      <input type="text">
    </app-accordion-item>

    <app-accordion-item  title="two" name="accordion2">
      content 22
    </app-accordion-item>
   

  `,
  styles: ``
})
export default class Uikit2Component {

}
