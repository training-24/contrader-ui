import { AfterViewInit, Component } from '@angular/core';
import { LeafletComponent } from '../uikit/components/leaflet.component';


@Component({
  selector: 'app-uikit5',
  standalone: true,
  imports: [
    LeafletComponent
  ],
  template: `
    <button (click)="zoomValue = zoomValue - 1">-</button>
    {{zoomValue}}
    <button (click)="zoomValue = zoomValue + 1">+</button>
    <button (click)="coords = [44, 14]">Location 1</button>
    <button (click)="coords = [45, 13]">Location 2</button>
    <button (click)="coords = [43, 11]">Location 3</button>
    <hr>
    <app-leaflet 
      [zoom]="zoomValue" 
      [coords]="coords"
    />
    
    <!--<app-leaflet />-->
  `,
  styles: `
  `
})
export default class Uikit5Component {
  zoomValue = 11;
  coords: [number, number] = [43, 13]
}
