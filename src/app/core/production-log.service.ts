import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductionLogService {
  counter = 1;
  logs: string[] = [];

  constructor() {
    console.log('PROD log service constructor')
  }

  show(msg: string) {
    this.logs.push('PROD ' + msg + Math.random())
    this.counter++;
  }

}
