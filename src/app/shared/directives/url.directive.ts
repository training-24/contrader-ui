import { Directive, ElementRef, HostBinding, HostListener, inject, input } from '@angular/core';

@Directive({
  selector: '[appUrl]',
  standalone: true
})
export class UrlDirective {
  appUrl = input.required<string>()
  @HostBinding() class = 'cursor-pointer underline'

  @HostListener('click')
  openURL() {
    window.open(this.appUrl())
  }

}

