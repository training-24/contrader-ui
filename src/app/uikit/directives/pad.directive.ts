import { computed, Directive, HostBinding, input, Input } from '@angular/core';

export type Size = 'sm' | 'xl';

@Directive({
  selector: '[appPad]',
  standalone: true
})
export class PadDirective {
  appPad = input<Size>()

  padding = computed(() => {
    return this.appPad() === 'sm' ? 'p-4' : 'p-20'
  })

  @HostBinding() get className() {
    return this.padding()
  }

 /* constructor() {
    console.log(this.appPad)
  }

  ngOnInit() {
    console.log(this.appPad)
  }
*/
}
