import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SuperLogService {
  counter = 1;
  logs: string[] = [];

  constructor() {
    console.log('SUPER log service constructor')
  }

  show(msg: string) {
    this.logs.push('super ' + msg + Math.random())
    this.counter++;
  }

  ngOnDestroy()  {
    console.log('super destroy')
  }
}
