import { HttpClient } from '@angular/common/http';
import { Inject, inject, Injectable } from '@angular/core';
import { CFG_VALUE } from '../app.component';
import { LogService } from './log.service';

@Injectable({
  providedIn: 'root'
})
export class ConfigService {
  http = inject(HttpClient)
  log = inject(LogService)
  cfgValue = inject(CFG_VALUE)

  constructor() {
    console.log('config service', this.cfgValue)
  }

  doSomething() {
    this.http.get('')
  }
}
