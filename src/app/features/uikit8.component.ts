import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { ConfigService } from '../core/config.service';
import { LogService } from '../core/log.service';

@Component({
  selector: 'app-uikit8',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <h1>UIKIT 8 PRODUCTION SERVICE</h1>
    <div>counter: {{logSrv.counter}}</div>
    <div>{{logSrv.logs | json}}</div>
    <button (click)="logSrv.show('item')">
      LOG
    </button>
  `,
  styles: ``
})
export default class Uikit8Component {
  logSrv = inject(LogService)
  cftSrv = inject(ConfigService)

  constructor() {
    this.cftSrv.doSomething()

  }

}
