import { provideHttpClient } from '@angular/common/http';
import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';
import { environment } from '../environments/environment';
import { CFG_VALUE } from './app.component';

import { routes } from './app.routes';
import { AuthService } from './core/auth.service';
import { ConfigService } from './core/config.service';
import { LogService } from './core/log.service';
import { ProductionLogService } from './core/production-log.service';

export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideHttpClient(),
    // AuthService,
    { provide: AuthService, useClass: AuthService },
    {
      provide: LogService, useFactory: () => {
        return environment.production ?
          new ProductionLogService() :
          new LogService()
      }
    },
    {
      provide: CFG_VALUE,
      useValue: 1234
    },
  ]
};
