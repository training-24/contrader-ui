import {
  booleanAttribute,
  ChangeDetectionStrategy,
  Component,
  computed,
  input,
  Input,
  numberAttribute, output
} from '@angular/core';

@Component({
  selector: 'app-picsum',
  standalone: true,
  imports: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <img [src]="url()" alt="">
  `,
  styles: ``
})
export class PicsumComponent {
  width = input(400, { transform: numberAttribute })
  height = input(300, { transform: numberAttribute })
  grayscale = input(false, { transform: booleanAttribute })
  // url = computed(() => `https://picsum.photos/${this.width()}/${this.height()}`)

  url = computed(() => {
    const w = this.width()
    const h = this.height()
    const grayscale = this.grayscale() ? '?grayscale' : ''
    return `https://picsum.photos/${w}/${h}${grayscale}`
  })

}
