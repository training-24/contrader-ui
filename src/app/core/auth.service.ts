import { computed, Injectable, signal } from '@angular/core';

export type Auth = {
  token: string;
  displayName: string;
  role: 'admin' | 'guest';
}

@Injectable()
export class AuthService {
  data: Auth | null = null;
  dataSignal = signal<Auth | null>(null)

  isLogged = computed(() => !!this.dataSignal())
  role = computed(() => this.dataSignal()?.role)
  displayName = computed(() => this.dataSignal()?.displayName)

  constructor() {
    console.log('wefe')
  }

  login() {
    // http....
    const auth: Auth = { token: 'abc', displayName: 'Fabio', role: 'guest'}
    this.data = auth;
    this.dataSignal.set(auth)
  }

  logout() {
    this.data = null;
    this.dataSignal.set(null)
  }

}
