import { Directive, effect, inject, input, TemplateRef, ViewContainerRef } from '@angular/core';
import { AuthService } from '../../core/auth.service';

@Directive({
  selector: '[appIfRoleIs]',
  standalone: true
})
export class IfRoleIsDirective {
  appIfRoleIs = input.required<string>()
  tpl = inject(TemplateRef)
  view = inject(ViewContainerRef)
  authService = inject(AuthService);

  constructor() {
    effect(() => {
      this.view.clear()

      const requiredRole = this.appIfRoleIs();

      if (
        this.authService.isLogged() &&
        requiredRole === this.authService.role()
      )
        this.view.createEmbeddedView(this.tpl)
    });
  }
}
