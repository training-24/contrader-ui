import { JsonPipe, NgClass } from '@angular/common';
import { booleanAttribute, Component, Input, OnInit } from '@angular/core';
import { data } from 'autoprefixer';

@Component({
  selector: 'app-timeline',
  standalone: true,
  imports: [
    JsonPipe,
    NgClass
  ],
  template: `
    <ul 
      class="timeline timeline-vertical"
      [ngClass]="{
        'sm:timeline-horizontal': !vertical,
      }"
    >
      @for (item of items; track $index) {
        <li>
          @if (!$first) {
            <hr/>
          }
          
          <div class="timeline-start">{{ item.start }}</div>
          <div class="timeline-middle">
            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" class="w-5 h-5"><path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.857-9.809a.75.75 0 00-1.214-.882l-3.483 4.79-1.88-1.88a.75.75 0 10-1.06 1.061l2.5 2.5a.75.75 0 001.137-.089l4-5.5z" clip-rule="evenodd" /></svg>
          </div>
          <div class="timeline-end timeline-box">{{ item.end }}</div>
          @if (!$last) {
            <hr/>
          }
        </li>
      }
    </ul>
    
  `,
  styles: ``
})
export class TimelineComponent implements OnInit {
  // data filtered
 /* @Input({ transform: (data: TimelineData[]) => {
    return data.filter(item => {
      return +item.start >= 2018
  })
  }}) items: TimelineData[] = [];*/

  @Input({ transform: (data: TimelineData[]) => {
      return data.filter(item => {
        return +item.start >= 2018
      })
    }}) items: TimelineData[] = [];

  @Input({ transform: booleanAttribute})  vertical: boolean = false;

  @Input() fn: ((a: number, b: number) => number ) | undefined;

  ngOnInit() {
    if(this.fn)
      console.log(this.fn(2, 3))
  }

}

export type TimelineData = {
  start:string,
  end: string
}
