import { Component, Input } from '@angular/core';
import { AccordionItemComponent } from './accordion-item.component';

@Component({
  selector: 'app-accordion',
  standalone: true,
  imports: [
    AccordionItemComponent
  ],
  template: `
    @for(item of items; track $index) {
      <app-accordion-item [title]="item.start">
        {{item.end}}
      </app-accordion-item>
    }
  `,
  styles: ``
})
export class AccordionComponent {
  @Input() items: any[] = []
}
