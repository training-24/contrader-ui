import { Component, effect, ElementRef, input, untracked, viewChild, } from '@angular/core';
import { LatLngExpression } from 'leaflet';
import L from 'leaflet';

@Component({
  selector: 'app-leaflet',
  standalone: true,
  imports: [],
  template: `
    <div #mapReference class="map"></div>
  `,
  styles: `
    .map { height: 180px }
  `
})
export class LeafletComponent  {
  mapRef = viewChild<ElementRef<HTMLElement>>('mapReference')
  zoom = input<number>(10);
  coords = input<LatLngExpression>();
  map!: L.Map ;

  constructor() {
    effect(() => {
      this.map = L.map(this.mapRef()!.nativeElement)
        .setView(
          untracked(() => this.coords()!),
          untracked(this.zoom)
        );

      L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        maxZoom: 19,
        attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
      }).addTo(this.map);
    });

    effect(() => {
      this.map.setZoom(this.zoom())
    });

    effect(() => {
      this.map.setView(this.coords()!)
    });
  }

  /*
  ngOnChanges(changes: SimpleChanges) {
    console.log(this.mapRef)
    if (!this.map) {
      this.initMap()
    }
    if (changes['zoom']) {
      this.map.setZoom(this.zoom())
    }
    if (changes['coords']) {
      this.map.setView(this.coords())
    }
  }

  ngOnInit() {
    if (!this.map) {
      this.initMap()
    }
    this.map.setZoom(this.zoom())
  }

  initMap() {
    this.map = L.map(this.mapRef?.nativeElement)
      .setView(this.coords(), this.zoom());

    L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 19,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    }).addTo(this.map);
  }

   */

}
