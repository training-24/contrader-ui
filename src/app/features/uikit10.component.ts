import { NgIf } from '@angular/common';
import {
  Component,
  effect,
  ElementRef,
  inject,
  TemplateRef,
  viewChild,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { CardComponent } from '../uikit/components/card.component';
import { Weather3SignalComponent } from '../uikit/components/weather3.component';

@Component({
  selector: 'app-uikit10',
  standalone: true,
  imports: [
    NgIf,
    CardComponent
  ],
  template: `
    <ng-template #tpl>
      ciao ciao ciao
    </ng-template>
    
    
    
    <p>
      uikit10 works!
    </p>
    <input type="text" #inputRef>
    
  `,
  styles: ``
})
export default class Uikit10Component {
  input = viewChild<ElementRef<HTMLInputElement>>('inputRef')
  tpl = viewChild<TemplateRef<any>>('tpl')
  card = viewChild<CardComponent>('card')

  view = inject(ViewContainerRef)


  constructor() {
    effect(() => {
      const tpl = this.tpl();
      if (tpl) {
        this.view.createEmbeddedView(tpl)
        this.view.createEmbeddedView(tpl)
        this.view.createEmbeddedView(tpl)
        this.view.createEmbeddedView(tpl)
      }


      const compo = this.view.createComponent(Weather3SignalComponent)
      compo.setInput('city', 'Trieste')
    });
  }
}
