import { DatePipe, JsonPipe } from '@angular/common';
import { ChangeDetectionStrategy, Component, computed, effect, inject } from '@angular/core';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { IfLoggedDirective } from '../shared/directives/if-logged.directive';
import { IfRoleIsDirective } from '../shared/directives/if-role-is.directive';
import { IfSignInDirective } from '../shared/directives/if-sign-in.directive';
import { MyRouterLinkDirective } from '../shared/directives/my-router-link.directive';
import { AuthService } from './auth.service';

@Component({
  selector: 'app-navbar',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    RouterLink,
    JsonPipe,
    RouterLinkActive,
    MyRouterLinkDirective,
    IfSignInDirective,
    IfLoggedDirective,
    IfRoleIsDirective,
    DatePipe
  ],
  template: `
    <!--COMPONENTS-->
    <button class="btn" routerLink="uikit1"  appMyRouterLink="btn-info">1</button>
    <button class="btn" routerLink="uikit2" routerLinkActive="btn-info">2</button>
    <button class="btn" routerLink="uikit3"  routerLinkActive="btn-info">3</button>
    <button class="btn" routerLink="uikit4"  routerLinkActive="btn-info">4</button>
    <button class="btn" routerLink="uikit5"  routerLinkActive="btn-info">5</button>
    <!--DI-->
    <button class="btn" routerLink="uikit6"  routerLinkActive="btn-info">6</button>
    <button class="btn" routerLink="uikit7"  routerLinkActive="btn-info">7</button>
    <button class="btn" routerLink="uikit8"  routerLinkActive="btn-info">8</button>
    <!--DIRECTIVES-->
    <button class="btn" routerLink="uikit9"  routerLinkActive="btn-info">9</button>
    <button class="btn" routerLink="uikit10"  routerLinkActive="btn-info">10</button>
    <button class="btn" routerLink="uikit11"  routerLinkActive="btn-info">11</button>
    <button class="btn" routerLink="uikit12"  routerLinkActive="btn-info">12</button>
    <button class="btn" routerLink="uikit13"  routerLinkActive="btn-info">13</button>
    <button class="btn" routerLink="login"  routerLinkActive="btn-info">login</button>

    @if (authSrv.isLogged()) {
      <button class="btn" (click)="authSrv.logout()">logout DI</button>
    }

    <button *appIfLogged class="btn" (click)="authSrv.logout()">logout *dir</button>
    <button appIfSignIn class="btn" (click)="authSrv.logout()">logout2 dir</button>
    
    {{ authSrv.dataSignal()?.displayName }}
    
    <button *appIfRoleIs="'admin'">CMS ADMIN</button>
    
    
    
  `,
  styles: ``
})
export class NavbarComponent {
  authSrv = inject(AuthService)

  constructor() {
    effect(() => {
     //  console.log(this.authSrv.isLogged())
    });
  }

}
