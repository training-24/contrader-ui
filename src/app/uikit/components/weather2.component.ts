import { JsonPipe, NgIf } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Component, inject, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { delay } from 'rxjs';
import { Meteo } from '../../model/meteo';

@Component({
  selector: 'app-weather2',
  standalone: true,
  imports: [
  ],
  template: `
    @if(meteo) {
      TEMPERATURE: {{ meteo.main.temp }}° in {{city}}
    }
  `,
  styles: ``
})
export class Weather2Component  implements OnChanges {
  meteo: any
  http = inject(HttpClient)

  @Input() city: string | undefined;
  @Input() unit: 'metric' | 'imperial' = 'metric'

  ngOnChanges(changes: SimpleChanges){
    if (changes['city']) {
      console.log('changed CITY', changes['city'].currentValue, this.city, this.unit)
    }
    this.http.get(`https://api.openweathermap.org/data/2.5/weather?q=${this.city}&units=${this.unit}&APPID=eb03b1f5e5afb5f4a4edb40c1ef2f534`)
      .subscribe(res => {
        this.meteo = res;
      })
  }


}
