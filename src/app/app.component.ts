import { HttpClient } from '@angular/common/http';
import { Component, InjectionToken } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';
import { environment } from '../environments/environment';
import { AuthService } from './core/auth.service';
import { ConfigService } from './core/config.service';
import { LogService } from './core/log.service';
import { NavbarComponent } from './core/navbar.component';
import { ProductionLogService } from './core/production-log.service';
import { SuperLogService } from './core/super-log.service';

export const CFG_VALUE = new InjectionToken<string>('ejhbwohoiwhorw')

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, RouterLink, NavbarComponent],
  template: `
  
    <app-navbar />
    <hr>
    <router-outlet />
  `,
  providers: [

  ],
  styles: [],
})
export class AppComponent {
  title = 'contrader-ui';
}
