import { NgForOf } from '@angular/common';
import { Component, inject, Optional } from '@angular/core';
import { CardService } from '../../core/card.service';

@Component({
  selector: 'app-card',
  standalone: true,
  imports: [
    NgForOf
  ],
  template: `
    <p>
      card works! {{title}}
    </p>
    <div
      class="bg-sky-400 rounded-xl p-2 m-1 inline-block text-white text-xs text-center"
      *ngFor="let item of items; let i = index"
    >{{i}}</div>
    
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adip
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab corporis culpa cum est et eveniet illum iste, nemo perspiciatis possimus vero voluptatibus. Ab dolore, ipsam itaque laudantium nulla similique vel.
  `,
  styles: ``,
  providers: []
})
export class CardComponent {
  cardSrv = inject(CardService, { optional: true })
  title: string = 'CARD TITLE'

  constructor() {
    console.log(this.cardSrv)
    if (this.cardSrv) {
      this.title = this.cardSrv.title
    }
  }
  items = new Array(40000)
}
