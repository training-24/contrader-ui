import { Routes } from '@angular/router';

export const routes: Routes = [
  // UIKIT: Components
  { path: 'uikit1', loadComponent: () => import('./features/uikit1.component') },
  { path: 'uikit2', loadComponent: () => import('./features/uikit2.component') },
  { path: 'uikit3', loadComponent: () => import('./features/uikit3.component') },
  { path: 'uikit4', loadComponent: () => import('./features/uikit4.component') },
  { path: 'uikit5', loadComponent: () => import('./features/uikit5.component') },

  // D.I. Examples
  { path: 'uikit6', loadComponent: () => import('./features/uikit6.component') },
  { path: 'uikit7', loadComponent: () => import('./features/uikit7.component') },
  { path: 'uikit8', loadComponent: () => import('./features/uikit8.component') },
  { path: 'uikit9', loadComponent: () => import('./features/uikit9.component') },
  { path: 'uikit10', loadComponent: () => import('./features/uikit10.component') },
  { path: 'uikit11', loadComponent: () => import('./features/uikit11.component') },
  { path: 'uikit12', loadComponent: () => import('./features/uikit12.component') },
  { path: 'uikit13', loadComponent: () => import('./features/uikit13.component') },
  { path: 'login', loadComponent: () => import('./features/login.component') },

  // Directives

  // Pipes
  { path: '', redirectTo: 'login', pathMatch: 'full'}
];
