import { JsonPipe } from '@angular/common';
import { Component, inject } from '@angular/core';
import { LogService } from '../core/log.service';

@Component({
  selector: 'app-uikit6',
  standalone: true,
  imports: [
    JsonPipe
  ],
  template: `
    <h1>Log Service</h1>
    <div>counter: {{logSrv.counter}}</div>
    <div>{{logSrv.logs | json}}</div>
    <button (click)="logSrv.show('item')">
      LOG
    </button>
  `,
  styles: ``,
  providers: [
    /*{ provide: LogService, useClass: LogService }*/
    /*{ provide: LogService, useClass: LogService }*/
    LogService,
  ]
})
export default class Uikit6Component {
  logSrv = inject(LogService)


}
