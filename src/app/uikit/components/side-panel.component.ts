import { NgClass } from '@angular/common';
import { Component, input, model, signal } from '@angular/core';

@Component({
  selector: 'app-side-panel',
  standalone: true,
  imports: [
    NgClass
  ],
  template: `
    <div 
      class="fixed bg-black text-white bg-opacity-90 top-0 bottom-0 w-48   transition-all duration-1000"
      [ngClass]="{
        '-left-0': open(),
        '-left-48': !open()
      }"
    >
      <button class="absolute top-2 right-2" (click)="open.set(false)">X</button>
      <h1>{{ title() }} {{open()}}</h1>

      <ng-content></ng-content>
    </div>
   
  `,
  styles: ``
})
export class SidePanelComponent {
  open = model(false)
  title = input('Side Panel')
}
